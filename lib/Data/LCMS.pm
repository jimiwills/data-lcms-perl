package Data::LCMS;

use strict;
use warnings;
use Carp;
use Scalar::Util qw /dualvar isdual/;

=pod

=head1 NAME

Data::LCMS

=head1 SYNOPSIS

use Data::LCMS qw/ppm_match create_matchable_ranges match_ranges/;

my $ranges = create_matchable_ranges(\@mzs, 'ppm'=>5);
my $matches = match_ranges($ranges, \@mzs2);

# or a shortcut for the above
my $matches = ppm_match(\@mzs, \@mzs2, $ppm); #


=head1 DESCRIPTION

Functions for manipulating and matching LCMS data in an efficient way.

Things like joining data are not straightforward with LCMS data, in which
the columns you want to join on may not match exactly.  In high-resolution
instruments, such as Orbitraps, the range across which you would want to 
match m/z (mass/charge ratio) is usually defined in parts-per-million 
(ppm) and so the size of the range changes with the m/z value.  

You may wish to match retention time (RT; also sometimes called elution
time: ET) and/or signal intensity (aka ion count, ion current.)  And what
do you want to do it more than one match exists?  Aggregate?  Choose the one
with the highest intensity, or the lowest ppm-error, or some measure of 
closeness that combines both m/z and RT?  

It's kinda complicated, and this functionality is only available in certain
specialised libraries, in R for example.  Even then, it's not always 
optimised for speed (in R we generally make a matrix and then find the
minimum errors, but this is slow and can require high memory for large
spectra or datasets.)

Finally, if you're working with the likes of Orbitrap data, you might be 
locked into working on Windows.  I'm aiming to make this module run within
Git Bash for Windows (from https://gitforwindows.org/ ) since it's the 
one Perl I know I have on every Windows computer I have access to, and 
it can be run standalone.

=over 4

=item new 

=back

Make a new object.

I quickly realise that you might want to read two files "at the same time"
and that they might require different parameters per line, so OOP is the 
way to go.

=cut

sub new {
	my $p = shift;
	my $c = ref($p) || $p;
	my $o = {};
	bless $o, $c;
	$o->init(@_);
	return $o;
}

=pod

=over 4

=item init 

=back

(re)initialise object.

Including recompiling the regex for field parsing.

=cut

sub init {
	my ($o, %opts) = @_;
	my %defaults = (
		'EOL' => qr([\r\n]*$),
		'SEP' => "\t",
		'QUOTE' => '"',
		'ESC' => '"',
	);
	foreach my $k(keys %defaults){
		$o->{$k} = $defaults{$k};
	}
	foreach my $k(keys %opts){
		$o->{$k} = $opts{$k};
	}
	$o->refresh_field_regex();
}

=pod

=over 4

=item refresh_field_regex $line

=back

Recalculates and stores the compiled regex used to parse fields in 
read_delim_field.

This is called by init() 

=cut

sub refresh_field_regex {
	my $o = shift;
	my ($EOL, $SEP, $QUOTE, $ESC) = map {$o->{$_}} qw/EOL SEP QUOTE ESC/;
	my $re = qr/^($QUOTE?)(|.*?(?!$ESC).)\1(?:$SEP|$EOL)(.*)/s;
	$o->{'FIELD_REGEX'} = $re;
}

=pod

=over 4

=item read_delim_field $string

=back

Returns a fields read from a line of a tsv, and the remainder of the line.
Is used by read_tsv_line.

Will remove quotes if they exist.  Will consider tabs and quotes within fields,
but will not unescape quotes within fields.

If you can, use Text::CSV instead!  This is here for the likes of 
Compound Discoverer output on systems where installing perl modules
is not possible.  Nevertheless, you can still use Text::CSV_PP on any
system (just download and use)

=cut


sub read_delim_field {
	my ($o,$string) = @_;
	my $re = $o->{'FIELD_REGEX'};
	if($string =~ /$re/){
		my ($field, $remainder) = ($2,$3);
		return ($field, $remainder); # field, remainder
	}
	else {
		confess "Could not parse line:\n``$string''\n with regex\n/$re/\n";
	}
}


=pod

=over 4

=item read_delim_line $filehandle

=back

Returns a reference to a list of fields read from a line of a tsv file.
See read_delim_field() for more info on object properties that impact
on this.

If you can, use Text::CSV instead!  This is here for the likes of 
Compound Discoverer output on systems where installing perl modules
is not possible.  Nevertheless, you can still use Text::CSV_PP on any
system (just download and use)

=cut

sub read_delim_line {
	my ($o,$fh) = @_;
	my $line = <$fh>;
	my $field;
	my @F = ();
	while($line){
		($field, $line) = $o->read_delim_field($line);
		push @F, $field;
	}
	return \@F;
}


=pod

=over 4

=item indexof $itemvalue, \@list

=back

Returns index of firts occurence of $itemvalue in @list,
or -1 if not there.  Can't really believe this isn't in 
List::Utils or something.

=cut

sub indexof {
	shift if ref($_[0]); # oop or func
	my($value, $list, $startat) = @_;
	$startat = 0 unless defined $startat;
	foreach my $i($startat..$#$list){
		return $i if $value eq $list->[$i];
	}
	return -1;
}

=pod

=over 4

=item dual_sort \@list

=back

Alters a list (of numbers) such that each member is a dualvar.
The numeric value is the number, by which the list is sorted,
and the string value is the original index of that item before sorting.

Returns the list of indices.

=cut

sub dual_sort {
	shift if ref($_[0]) && ref($_[0]) eq 'Data::LCMS'; # oop or func
	my($list) = @_;
	foreach my $i(0..$#$list){
		$list->[$i] = dualvar($list->[$i], "$i");
	}
	@$list = sort {$a<=>$b} @$list;
	return [map {"$_"} @$list];
}

=pod

=over 4

=item to_ppm_ranges \@list, $ppm

=back

Returns an even list of pairs, each the lower and upper
bound of the range indicated by $ppm for that specific m/z value.

If ppm is negative, its positive version is used as the absolute range radius.

=cut

sub to_ppm_ranges {
	shift if ref($_[0]) && ref($_[0]) eq 'Data::LCMS'; # oop or func
	my($list, $ppm) = @_;
	# only do the division once...
	my $factor = $ppm/1e6;
	# a quick check...
	confess "to_ppm_ranges expects a reference to a list"
		." of dualvars as its firts argument. Try generating on with dual_sort()."
		unless isdual($list->[0]);
	my @ranges = ();
	foreach my $i(0..$#$list){
		my $mz = $list->[$i];
		my $idx = "".$list->[$i];
		my $delta = $ppm < 0 ? 0-$ppm : $mz * $factor;
		push @ranges, dualvar($mz-$delta, $idx), dualvar($mz+$delta, $mz);
	}
	return \@ranges;
}


=pod

=over 4

=item to_abs_ranges \@list, $abs

=back

Returns an even list of pairs, each the lower and upper
bound of the range indicated by $abs.

A shortcut to to_ppm_ranges(\@list, -$abs)

=cut

sub to_abs_ranges {
	shift if ref($_[0]) && ref($_[0]) eq 'Data::LCMS'; # oop or func
	my($list, $abs) = @_;
	return to_ppm_ranges($list, -$abs)
}


=pod

=over 4

=item match_ranges_values \@ranges, \@values

=back

Returns a listref of pairs of indices for the matches.
Also, each is a dual var with more infos in the string.

=cut

sub match_ranges_values {
	shift if ref($_[0]) && ref($_[0]) eq 'Data::LCMS'; # oop or func
	my ($ranges,$mzs) = @_;
	# a quick check...
	confess "match_ranges_mzs expects ranges from to_ppm_ranges and sorted mzs from dual_sort."
		unless isdual($mzs->[0]) && isdual($ranges->[0]);

	my @matches = ();
	my $pi = 0;
	my $ri = 0;
	while($pi < @$ranges && $ri < @$mzs){
		if($ranges->[$pi+1] < $mzs->[$ri]){
			# hi of pair less than ref, inc pair
			$pi+=2;
		}
		elsif($mzs->[$ri] < $ranges->[$pi]){
			# ref less than lo of pair, inc ref
			$ri++;
		}
		else {
			# match, report and inc both
			# capture all values that fit this range...
			my $i = 0;
			while($ri+$i < @$mzs && $mzs->[$ri+$i] <= $ranges->[$pi+1]){
				push @matches, (
					dualvar($ranges->[$pi].'', ($pi/2)."($ranges->[$pi]): ".($ranges->[$pi]+0).", ".($ranges->[$pi+1]+0)), 
					dualvar($mzs->[$ri+$i].'', ($ri+$i)."($mzs->[$ri+$i]): ".($mzs->[$ri+$i]+0))
				);
				$i++;
			}
			# now, increment range, but not the value...
			$pi+=2;
		}
	}
	return \@matches;
}
=pod

=over 4

=item logical_and_matches $matches1, $matches2

=back

Returns a match list of those matches that exists in both inputs.
Obviously, they must both refer to the same two sets of data
(because it's the indices that are compared)

e.g.

if $x = [1, 2, 3, 4, 5, 6];
and $y= [1, 1, 3, 4, 5, 5];
then the output would be just [3, 4]
since that's the only pair resent in both.

=cut

sub logical_and_matches {
	shift if ref($_[0]) && ref($_[0]) eq 'Data::LCMS'; # oop or func
	my ($m1, $m2) = @_;
	# first build a hashref of $m2 ... so m2 should ideally be the smaller set
	my %m2 = ();
	for(my $i=0; $i<@$m2; $i+=2){
		my $x = $m2->[$i]+0;
		my $y = $m2->[$i+1]+0;
		$m2{"$x,$y"} = @_;
	}
	# then use it to filter $m1
	my @matches = ();
	for(my $i=0; $i<@$m1; $i+=2){
		my $x = $m1->[$i]+0;
		my $y = $m1->[$i+1]+0;
		if(exists $m2{"$x,$y"}){
			push @matches, $x, $y;
		}
	}
	return \@matches;
}

=pod

=over 4

=item aggregate_match_indices \@matches

=back

Returns a structure reflecting aggregation of matches.

=cut

sub aggregate_match_indices {
	shift if ref($_[0]) && ref($_[0]) eq 'Data::LCMS'; # oop or func
	my ($matches) = @_;
	my %revaggs = ();
	for(my $i=0; $i<@$matches; $i+=2){
		my $x = $matches->[$i]+0;
		my $y = $matches->[$i+1]+0;
		$revaggs{$y} = [] unless defined $revaggs{$y};
		$revaggs{$y} = [sort {$a<=>$b} (@{$revaggs{$y}}, $x)];
	}
	my %aggregated = ();
	for(my $i=0; $i<@$matches; $i+=2){
		my $x = $matches->[$i]+0;
		my $y = $matches->[$i+1]+0;
		$aggregated{$x} = {} unless defined $aggregated{$x};
		$aggregated{$x}->{$y} = $revaggs{$y};
	}
	return \%aggregated;
}


=pod

=over 4

=item read_cols $filename, @column_names

=back

Returns a structure reflecting aggregation of matches.

=cut

sub read_cols {
	my ($o, $fn, @colnames) = @_;
	my $fh = IO::File->new($fn, 'r') or confess "Could not read $fn: $!";
	my $head = $o->read_delim_line($fh);
	my @indices = ();
	my %cols = ();
	foreach my $cn(@colnames){
		my $idx = indexof($cn, $head);
		die "Could not find column '$cn' in file '$fn'"
			if $idx < 0;
		push @indices, $idx;
		$cols{$cn} = [];
	}

	while(! eof($fh)){
		my $fields = $o->read_delim_line($fh);
		for(my $i=0; $i<@indices; $i++){
			push @{$cols{$colnames[$i]}}, $fields->[$indices[$i]];
		}
	}
	$fh->close();
	return \%cols;
}

=pod

=over 4

=item match_files $filename1 => $filename2, $col1_1=>$col2_1,$ppm_1,  ...

=back

Returns a structure reflecting logically ANDed matches from two files.

=cut

sub match_files {
	my ($o, $fn1, $fn2, @defs) = @_;
	my @colnames1 = ();
	my @colnames2 = ();
	my @triplets = ();
	my @ppms = ();
	while(@defs){
		print "$defs[0], $defs[1], $defs[2] \n";
		push @colnames1, shift @defs;
		push @colnames2, shift @defs;
		push @ppms, shift @defs;
	}
	print join("\t", $fn1, @colnames1)."\n";
	print join("\t", $fn2, @colnames2)."\n";
	my $columns1 = $o->read_cols($fn1, @colnames1);
	my $columns2 = $o->read_cols($fn2, @colnames2);
	# okay, got all data needed from files now...
	my $logically_anded_matches;
	foreach my $i(0..$#colnames1){
		my $col1 = $columns1->{$colnames1[$i]};
		my $col2 = $columns2->{$colnames2[$i]};
		my $ppm = $ppms[$i];
		dual_sort($col1);
		dual_sort($col2);
		my $ranges = to_ppm_ranges($col1, $ppm);
		my $matches = match_ranges_values($ranges, $col2);
		if(! defined $logically_anded_matches){
			$logically_anded_matches = $matches;
		}
		else {
			$logically_anded_matches = logical_and_matches($logically_anded_matches, $matches);
		}
	}
	return $logically_anded_matches;
}

=pod

=over 4

=item match_files $filename1 => $filename2, $col1_1=>$col2_1,$ppm_1,  ...

=back

Returns a structure reflecting logically ANDed matches from two files.

=cut

sub output_matched_files {
	my ($o, $fn, $matches,
		$fn1, $prepend1, $colnames1,
		$fn2, $prepend2, $colnames2, $sep) = @_;
	my $fo = IO::File->new($fn, 'w') or confess "Could not write $fn: $!";

	if(! ref($colnames1)){
		# we are reading through file 1 and outputting everything
		my $fh = IO::File->new($fn1, 'r') or confess "Could not read $fn1: $!";
		my $agg = aggregate_match_indices($matches);
		my $head1 = $o->read_delim_line($fh);
		print $fo join ("\t", map {qq/"$_"/}
			$fn1, $fn2, 
			(map {"$prepend1$_"} @$head1),
			(map {"$prepend2$_"} @$colnames2)
		)."\n";

		my $columns2 = $o->read_cols($fn2, @$colnames2);

		my $i = 0;
		while(! eof($fh)){
			my $f = $o->read_delim_line($fh);
			if(exists $agg->{$i}){
				if(defined $sep){
					my @keys = keys %{$agg->{$i}};
					my $first = shift @keys;
					my $js = $first;
					my @vals = map {$columns2->{$_}->[$first]} @$colnames2;
					foreach my $j(@keys){
						my @morevals = map {$columns2->{$_}->[$j]} @$colnames2;
						foreach my $k(0..$#vals){
							$vals[$k] .= $sep . $morevals[$k];
						}
						$js .= $sep . $j;
					}
					my @out = ($i, $js, @$f,@vals);
					print $fo join("\t", @out)."\n";
				}
				else {
					foreach my $j(keys %{$agg->{$i}}){
						my @out = ($i, $j, @$f,
							map {$columns2->{$_}->[$j]} @$colnames2);
						print $fo join("\t", @out)."\n";
					}
				}
			}
			else {
					my @out = ($i, '', @$f,
						map {''} @$colnames2);
					print $fo join("\t", @out)."\n";
			}
			$i++;
		}
	}
	else {
		confess "selecting columns from file1 not yet implemented!";
	}
	$fo->close();
	return 1;
}


=pod


=over 4

=item residues 

=back

Returns hash of amino acid residue m/z values.

=cut

sub residues {
	my %residues = (
		'A'	=>	71.03711,
		'R'	=>	156.10111,
		'N'	=>	114.04293,
		'D'	=>	115.02694,
		'C'	=>	103.00919,
		'E'	=>	129.04259,
		'Q'	=>	128.05858,
		'G'	=>	57.02146,
		'H'	=>	137.05891,
		'L'	=>	113.08406,
		'K'	=>	128.09496,
		'M'	=>	131.04049,
		'F'	=>	147.06841,
		'P'	=>	97.05276,
		'S'	=>	87.03203,
		'T'	=>	101.04768,
		'W'	=>	186.07931,
		'Y'	=>	163.06333,
		'V'	=>	99.06841
	);
	return %residues;
}

=pod






=head1 AUTHOR

Jimi Carlo Wills (aka Jimi-Carlo Bukowski-Wills)

=head1 SEE ALSO



=cut




1;
