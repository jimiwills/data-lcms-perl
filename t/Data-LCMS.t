#!/usr/bin/perl
use strict;
use warnings;

use Test::More tests => 56;

require_ok( 'IO::File' );
require_ok( 'Data::LCMS' );
use_ok( 'Scalar::Util', 'isdual' );

ok(my $lcms = Data::LCMS->new());

my $test_line  = join("\t", qw/Unquoted "Quoted" "Tab Within" "Quote""Within"/)."\r\n";

foreach my $value(('Unquoted','Quoted',"Tab\tWithin",'Quote""Within')){
	my ($field, $remainder) = $lcms->read_delim_field($test_line);
	ok($value eq $field, "Test reading '$value' (got '$field')\n from '$test_line'");
	$test_line = $remainder;
}
## 8 tests

ok(my $fh = IO::File->new('t/test.tsv', 'r'),
	'Open test.tsv test file');

ok(my $row = $lcms->read_delim_line($fh),
	'Read a line');

$fh->close();

foreach my $value(('Unquoted','Quoted',"Tab\tWithin",'Quote""Within')){
	my $got = shift @$row;
	ok($value eq $got, "checking 'value' eq '$got' from field in file")
}
## 14 tests

my @indexof_items = qw/zero one two three four/;
my @indexof_notthere = qw/five six/;
my @indexof_test = (@indexof_items, @indexof_notthere);
my @indexof_expected_result = (0..4,-1,-1);

foreach my $i(0..$#indexof_test){
	my $item = $indexof_test[$i];
	my $exp = $indexof_expected_result[$i];
	my $got = $lcms->indexof($item, \@indexof_items);
	ok($got == $exp, "index of gives correct indices: $exp==$got");
}
## 21 tests

my @ppm = (1..8);
my @mz1 = qw(
	216.03981
	229.13661
	204.11082
	88.05234
	236.14395
	207.07551
	243.14714
	260.15263
	204.11084
);
my @mz2 = qw(
	216.040026
	229.1370683
	204.1114323
	88.05269221
	236.1451307
	207.0767525
	243.148842
	260.1547112
	88.0527
);

my $dualsortindex = Data::LCMS::dual_sort(\@mz1);
ok(isdual($mz1[0]), "dual worked");
is($mz1[0], 3, "testing dual sort $mz1[0] == 3 ");
is($mz1[1], 2, "testing dual sort $mz1[1] == 2 ");
is($mz1[8], 7, "testing dual sort $mz1[8] == 7");
is($dualsortindex->[0], 3, "testing dual sort [0] eq 3 ");
is($dualsortindex->[1], 2, "testing dual sort [1] eq 2 ");
is($dualsortindex->[8], 7, "testing dual sort [8] eq 7");

my $dualsortindex2 = Data::LCMS::dual_sort(\@mz2);

my $ppm = 4.1;
my $ranges = Data::LCMS::to_ppm_ranges(\@mz1, $ppm);
is(sprintf("%.4f", $ranges->[1]), 
	sprintf("%.4f", $mz1[0] * (1 + 4.1/1e6)),
	"check upper value");
is(sprintf("%.4f", $ranges->[0]), 
	sprintf("%.4f", $mz1[0] * (1 - 4.1/1e6)),
	"check lower value");
is($ranges->[1], $mz1[0], "dualvar mz");
is($ranges->[0], 3, "dualvar index");

my $matches = Data::LCMS::match_ranges_values($ranges, \@mz2);
is(@$matches, 6 * 2, "match count"); # 6 matches within 4.1ppm
# expected values:
my @expected_match_incides = qw(
	3	3
	3	8
	2	2
	8	2
	0	0
	1	1	
);
foreach my $i (0..$#expected_match_incides){
	is($matches->[$i]+0, $expected_match_incides[$i], "match indices $i")
}
# 45 tests to this point

my $expected_aggregate1 = {
	'3'=>{'3'=>[3], '8'=>[3]},
	'2'=>{'2'=>[2,8]},
	'8'=>{'2'=>[2,8]},
	'0'=>{'0'=>[0]},
	'1'=>{'1'=>[1]},
};

my $agg = Data::LCMS::aggregate_match_indices($matches);

is_deeply($agg, $expected_aggregate1, 'aggregation');


my @rt1 = qw(
	1 2 3 4 5 6 7 8 9
);
my @rt2 = qw(
	1 2 3 4 5 6 7 8 2
);
my $absrttol = 2;

my $dualsortindex3 = Data::LCMS::dual_sort(\@rt1);
my $dualsortindex4 = Data::LCMS::dual_sort(\@rt2);


my $rtranges = Data::LCMS::to_abs_ranges(\@rt1, $absrttol);
is($rtranges->[0]+0, -1, "rt ranges");
is($rtranges->[16]+0, 7, "rt ranges");

my $rtmatches = Data::LCMS::match_ranges_values($rtranges, \@rt2);
is(@$rtmatches, 40 * 2, "match count"); # 40 matches within 2minutes

my $expected_xref = [qw/
	0	0
	1	1
	2	2
	3	8
	3	3
/];
my $xref = Data::LCMS::logical_and_matches($rtmatches, $matches);

is_deeply($xref, $expected_xref, 'match && rtmatch deep');


my $expectedcols = {
	'Unquoted' => [1..4],
	"Tab\tWithin" => [9..12],
};

my $cols = $lcms->read_cols('t/test.tsv','Unquoted',"Tab\tWithin");

is_deeply($cols, $expectedcols, 'read_cols');


my $matched_files = $lcms->match_files(
	't/test.tsv' => 't/test.tsv',
	'Unquoted' => 'Quoted', 10, #ppm
	"Tab\tWithin" => 'Quote""Within', 10, #ppm
);
my $matched_files_expected = [1,1,2,2]; 
# because 0 mismatches in one pair of columns
# and 3 mismatches in the other
is_deeply($matched_files, $matched_files_expected, 'matched files');

my $outputfilename = 't/testout.tsv';
ok($lcms->output_matched_files(
	$outputfilename,
	$matched_files,
	't/test.tsv', '', '',
	't/test.tsv', 'file2_', ['Unquoted'],
), 'output file');


my $expected_header = ['t/test.tsv', 't/test.tsv', 'Unquoted', 'Quoted', "Tab\tWithin", 'Quote""Within', 'file2_Unquoted'];
my $expected_Quoted_col = [1,2,3,12];
my $expected_file2_Unquoted_col = ['',2,3,''];

my $fh = IO::File->new($outputfilename, 'r') or die "Could not read $outputfilename: $!";
my $outheader = $lcms->read_delim_line($fh);
$fh->close();

is_deeply($outheader, $expected_header, 'joined output header');

my $cols = $lcms->read_cols($outputfilename,'Quoted',"file2_Unquoted");

use Data::Dumper;
print Dumper $cols;

is_deeply($cols->{'Quoted'}, $expected_Quoted_col, 'left file column');
is_deeply($cols->{'file2_Unquoted'}, $expected_file2_Unquoted_col, 'right file column');

