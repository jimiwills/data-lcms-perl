# Data-LCMS-perl

Functions for manipulating and matching LCMS data in an efficient way.

Things like joining data are not straightforward with LCMS data, in which
the columns you want to join on may not match exactly.  In high-resolution
instruments, such as Orbitrap